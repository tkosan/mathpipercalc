package org.mathpiper.ui.gui.android;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.mathpiper.builtin.BuiltinFunction;
import org.mathpiper.builtin.BuiltinFunctionEvaluator;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.mathpipercalc0.R;
import org.mathpiper.test.Fold;

import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;


public class ProgramActivity extends ActionBarActivity implements ResponseListener{
    private Interpreter interpreter;

    private static final String TAG = "MathPiperPrgActivity";

    private Map<String, Fold> foldsMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program);



        interpreter = org.mathpiper.interpreters.Interpreters.getAsynchronousInterpreter();

        interpreter.addResponseListener(this);



        AskForNumber askForNumber = new AskForNumber();
        try {
            askForNumber.plugIn(interpreter.getEnvironment(), ProgramActivity.this);
        }catch(Throwable t)
        {
            Toast.makeText(getApplicationContext(), "AskForNumber not initialized.", Toast.LENGTH_SHORT).show();
        }




        initialize();
    }




    public synchronized EvaluationResponse evaluate(String input) {

        input = input.replace("\\", "\\\\");
        input = input.replace("\"", "\\\"");

        String inputText = "LoadScript(\"" + input + "\");";

        EvaluationResponse evaluationResponse = interpreter.evaluate(inputText);

        return evaluationResponse;
    }




    private void initialize() {

        // Unassign all variables in the MathPiper interpreter.
        EvaluationResponse response = evaluate("Unassign(*);");
        String result;

        if (response.isExceptionThrown()) {
            result = response.getException().getMessage();

            Log.e(TAG, result);

            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
            return;
        } else {
            result = response.getResult();
        }

        InputStream inputStream;
        try {

            String mpwFile = this.getIntent().getStringExtra(
                    "org.mathpiper.ui.gui.mpwsFile");

            // String mpwFile = "remember_numbers.mpws";

            inputStream = getAssets().open(mpwFile);

            foldsMap = org.mathpiper.test.MPWSFile.getFoldsMap(mpwFile,
                    inputStream);

        } catch (Throwable e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e(TAG, e.getMessage());
        }

        // =================== Configure.

        Fold fold = foldsMap.get("configuration");

        if (fold == null) {
            String message = "Configuration information is missing.";

            Log.e(TAG, message);

            Toast.makeText(ProgramActivity.this, message,
                    Toast.LENGTH_SHORT).show();

            return;
        }

        String codeText = fold.getContents();

        response = evaluate(codeText);

        if (response.isExceptionThrown()) {
            result = response.getException().getMessage();

            Log.e(TAG, result);

            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
            return;
        } else {
            result = response.getResult();
        }

        // =================== Information.
        fold = foldsMap.get("information");

        if (fold == null) {
            String message = "Exercise information is missing.";

            Log.e(TAG, message);

            Toast.makeText(ProgramActivity.this, message,
                    Toast.LENGTH_SHORT).show();

            return;
        }

        codeText = fold.getContents();

        response = evaluate(codeText);

        if (response.isExceptionThrown()) {
            result = response.getException().getMessage();

            Log.e(TAG, result);

            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
            return;
        } else {
            result = response.getResult();
        }

        // ================== program.
        fold = foldsMap.get("Program");

        if (fold == null) {
            String message = "The program is missing.";

            Log.e(TAG, message);

            Toast.makeText(ProgramActivity.this, message,
                    Toast.LENGTH_SHORT).show();

            return;
        }

        codeText = fold.getContents();

        codeText = codeText.replace("\\", "\\\\");
        codeText = codeText.replace("\"", "\\\"");

        String inputText = "LoadScript(\"" + codeText + "\");";

        interpreter.evaluate(inputText, true);


        // log("[\"Tag\",\"VersionInfo\"],[\"AppVersion\",\"" + appVersion +"\"],[\"MathpiperVersion\",\"" + org.mathpiper.Version.version() + "\"]");
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    @Override
    public void response(EvaluationResponse evaluationResponse) {

        if (evaluationResponse.isExceptionThrown()) {
            String result = evaluationResponse.getException().getMessage();

            Log.e(TAG, result);

            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();

            return;

        } else {
            String result = evaluationResponse.getResult();

            final TextView textView = (TextView) this.findViewById(R.id.programTextOutput);



            final String finalResult = result;
            runOnUiThread(new Runnable() {
                public void run()
                {
                    textView.append(finalResult + "\n");
                }
            });

           /* final String finalResult = result;
            runOnUiThread(new Runnable() {
                public void run()
                {
                    Toast.makeText(getApplicationContext(), finalResult, Toast.LENGTH_SHORT).show();
                }
            });*/
        }



    }

    @Override
    public boolean remove() {
        return false;
    }





    private class AskForNumber extends BuiltinFunction {
        private Context context;

        public void plugIn(org.mathpiper.lisp.Environment aEnvironment, Context context) throws Throwable {
            this.functionName = "AskForNumber";
            aEnvironment.getBuiltinFunctions().put(
                    this.functionName, new BuiltinFunctionEvaluator(this, 1, BuiltinFunctionEvaluator.FixedNumberOfArguments | BuiltinFunctionEvaluator.EvaluateArguments));

            this.context = context;
        }//end method.

        public void evaluate(org.mathpiper.lisp.Environment aEnvironment, int aStackTop) throws Throwable {
            if (getArgument(aEnvironment, aStackTop, 1) == null) {
                LispError.checkArgument(aEnvironment, aStackTop, 1);
            }


            Object argument = getArgument(aEnvironment, aStackTop, 1).car();


            if (argument == null) {
                LispError.checkArgument(aEnvironment, aStackTop, 1);
            }


            if(argument instanceof String) {


                final AtomicReference<String> userInput = new AtomicReference<String>();

                userInput.set("NO_INPUT_RECEIVED");

                final String messageStringFinal = Utility.stripEndQuotesIfPresent((String) argument);

                final AlertDialog.Builder builder = new AlertDialog.Builder(ProgramActivity.this);
                builder.setTitle(messageStringFinal);


                final EditText input = new EditText(this.context);

                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                builder.setView(input);


                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        userInput.set(input.getText().toString());
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });


                runOnUiThread(new Runnable() {
                    public void run()
                    {
                        builder.show();
                    }
                });


                while(userInput.get() != null && ((String)userInput.get()).equals("NO_INPUT_RECEIVED"))
                {
                    try
                    {
                        Thread.sleep(100);
                    }
                    catch(InterruptedException e)
                    {
                        //Eat exception.
                    }
                }






                setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aStackTop, userInput.get()));



            }




        }//end method.
    }//end class.
}