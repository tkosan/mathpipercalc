package org.mathpiper.mathpipercalc0.mathdroid;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

import org.mathpiper.mathpipercalc0.R;

/**
 * TODO: document your custom view class.
 */
public class ButtonsView extends LinearLayout {

    private LinearLayout horizontalLayout = null;

    public ButtonsView(Context context) {
        super(context);

        init(context);
    }

    public ButtonsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ButtonsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {

        this.setOrientation(LinearLayout.VERTICAL);
        this.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        LinearLayout horizontalLayout = new LinearLayout(context);
        horizontalLayout.setOrientation(LinearLayout.HORIZONTAL);
        horizontalLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        this.addView(horizontalLayout);

    }

    public void addButton(Button button)
    {
        this.horizontalLayout.addView(button);
    }

}
