package org.mathpiper.mathpipercalc0.mathdroid.history;


import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class HistoryProgramItems implements HistoryItem {
    private List<String> items = new ArrayList<String>();


    public HistoryProgramItems() {

    }


    public HistoryProgramItems(String item) {
        this.items.add(item);
    }


    public void add(String item) {
        this.items.add(item);
    }


    public String[] getAll() {
        return items.toArray(new String[items.size()]);
    }

    public String getFirst()
    {
        if(items.size() > 0) {
            return items.get(items.size()-1).split(":")[0];
        }
        else
        {
            return "ERROR";
        }
    }

    public String getSecond()
    {
        return "ERROR";
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int index = 0; index < items.size(); index++) {
            sb.append(items.get(index));

            if(index < items.size()-1) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }


    public String prettyPrint() {
        StringBuilder sb = new StringBuilder();

        for (int index = 0; index < items.size(); index++) {
            sb.append(items.get(index).split(":")[0]);

            if(index < items.size()-1) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }



}