package org.mathpiper.mathpipercalc0.mathdroid.history;

/**
 * At the moment, a history item is a "question" (the user's input) and a Node "answer".
 */
public class HistoryCalculationItem implements HistoryItem {
    private String question;
    private String answer;

    public HistoryCalculationItem(String question, String answer) {
        this.question = question;
        this.answer = answer;
    }


    @Override
    public String toString() {
        return question + "\n = " + answer;
    }

    public String prettyPrint() {
        return toString();
    }

    public String getFirst() {
        return question;
    }

    public String getSecond() {
        return answer;
    }
}
