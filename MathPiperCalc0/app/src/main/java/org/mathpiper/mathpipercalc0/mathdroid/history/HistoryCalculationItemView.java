package org.mathpiper.mathpipercalc0.mathdroid.history;

import android.content.*;
import android.widget.*;

import org.mathpiper.mathpipercalc0.R;

/**
 * A view for the items in the history transcript.
 * Basically just two text views: one for the question, another for the answer.
 */
public class HistoryCalculationItemView extends LinearLayout {
    private final TextView mQuestionView;
    private final TextView mAnswerView;

    public HistoryCalculationItemView(Context context, HistoryCalculationItem item) {
        super(context);

        this.setOrientation(VERTICAL);

        mQuestionView = new TextView(context);
        mQuestionView.setTextAppearance(context, R.style.history_question_appearance);
        mQuestionView.setText(item.getFirst());
        addView(mQuestionView, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

        mAnswerView = new TextView(context);
        mAnswerView.setTextAppearance(context, R.style.history_answer_appearance);
        mAnswerView.setText(" = " + item.getSecond());
        addView(mAnswerView, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    }

    // Allows reuse of a recycled HistoryItemView.
    public void setItem(HistoryCalculationItem item) {
        mQuestionView.setText(item.getFirst());
        mAnswerView.setText(" = " + item.getSecond());
    }
}
