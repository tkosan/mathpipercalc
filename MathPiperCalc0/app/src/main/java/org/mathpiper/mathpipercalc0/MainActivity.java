package org.mathpiper.mathpipercalc0;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.mathpiper.builtin.BuiltinFunction;
import org.mathpiper.builtin.BuiltinFunctionEvaluator;
import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.atomic.AtomicReference;


public class MainActivity extends ActionBarActivity implements ResponseListener{
    private Interpreter interpreter;
    private CodeEditor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editor = (CodeEditor) findViewById(R.id.messageText);


        interpreter = org.mathpiper.interpreters.Interpreters.getAsynchronousInterpreter();

        interpreter.addResponseListener(this);



        AskForNumber askForNumber = new AskForNumber();
        try {
            askForNumber.plugIn(interpreter.getEnvironment(), MainActivity.this);
        }catch(Throwable t)
        {
            Toast.makeText(getApplicationContext(), "AskForNumber not initialized.", Toast.LENGTH_SHORT).show();
        }


        final Button button_backspace = (Button) findViewById(R.id.button);
        button_backspace.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                String code = editor.getCleanText();

                code = code.replace("\\", "\\\\");
                code = code.replace("\"", "\\\"");

                String allCode = "LoadScript(\"" + code + "\");";

                //String allCode = "LoadScript(\"a := 1;b:=2;c:=AskForNumber(\\\"Input\\\");a + c;\");";

                interpreter.evaluate(allCode, true);


                //interpreter.evaluate("AskForNumber(\"Input\");", true);

                //interpreter.evaluate("2+3;", true);


            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    @Override
    public void response(EvaluationResponse evaluationResponse) {

        final EvaluationResponse evaluationResponseFinal = evaluationResponse;
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText(getApplicationContext(), evaluationResponseFinal.getResult(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean remove() {
        return false;
    }





    private class AskForNumber extends BuiltinFunction {
        private Context context;

        public void plugIn(org.mathpiper.lisp.Environment aEnvironment, Context context) throws Throwable {
            this.functionName = "AskForNumber";
            aEnvironment.getBuiltinFunctions().put(
                    this.functionName, new BuiltinFunctionEvaluator(this, 1, BuiltinFunctionEvaluator.FixedNumberOfArguments | BuiltinFunctionEvaluator.EvaluateArguments));

            this.context = context;
        }//end method.

        public void evaluate(org.mathpiper.lisp.Environment aEnvironment, int aStackTop) throws Throwable {
            if (getArgument(aEnvironment, aStackTop, 1) == null) {
                LispError.checkArgument(aEnvironment, aStackTop, 1);
            }


            Object argument = getArgument(aEnvironment, aStackTop, 1).car();


            if (argument == null) {
                LispError.checkArgument(aEnvironment, aStackTop, 1);
            }


            if(argument instanceof String) {


                final AtomicReference<String> userInput = new AtomicReference<String>();

                userInput.set("NO_INPUT_RECEIVED");

                final String messageStringFinal = Utility.stripEndQuotesIfPresent((String) argument);

                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Title");


                final EditText input = new EditText(this.context);

                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                builder.setView(input);


                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        userInput.set(input.getText().toString());
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });


                runOnUiThread(new Runnable() {
                    public void run()
                    {
                        builder.show();
                    }
                });


                while(userInput.get() != null && ((String)userInput.get()).equals("NO_INPUT_RECEIVED"))
                {
                    try
                    {
                        Thread.sleep(100);
                    }
                    catch(InterruptedException e)
                    {
                        //Eat exception.
                    }
                }






                setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aStackTop, userInput.get()));



            }




        }//end method.
    }//end class.
}